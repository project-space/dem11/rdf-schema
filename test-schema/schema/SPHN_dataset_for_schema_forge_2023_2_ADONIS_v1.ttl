@prefix : <https://www.biomedit.ch/rdf/sphn-ontology/adonis/2023/2#> .
@prefix adonis: <https://www.biomedit.ch/rdf/sphn-ontology/adonis/2023/2#> .
@prefix dc: <http://purl.org/dc/elements/1.1/> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix snomed: <http://snomed.info/id/> .
@prefix sphn: <https://biomedit.ch/rdf/sphn-ontology/sphn#> .
@prefix sphn-geno: <https://biomedit.ch/rdf/sphn-resource/geno/> .
@prefix sphn-hgnc: <https://biomedit.ch/rdf/sphn-resource/hgnc/> .
@prefix sphn-so: <https://biomedit.ch/rdf/sphn-resource/so/> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

sphn:hasCode rdfs:domain [ a owl:Class ;
            owl:unionOf ( sphn:AdministrativeGender sphn:AdverseEvent sphn:Allergen sphn:BodyPosition sphn:BodySite sphn:Chromosome sphn:CivilStatus sphn:DataProviderInstitute sphn:DeathStatus sphn:Diagnosis sphn:DiagnosticRadiologicExamination sphn:ElectrocardiographicProcedure sphn:FOPHDiagnosis sphn:FOPHProcedure sphn:Gene sphn:Intent sphn:LabTest sphn:Laterality sphn:MeasurementMethod sphn:NursingDiagnosis sphn:Organism sphn:PharmaceuticalDoseForm sphn:PhysiologicState sphn:ProblemCondition sphn:Procedure sphn:Protein sphn:RadiotherapyProcedure sphn:Reference sphn:Substance sphn:Transcript sphn:TumorGrade sphn:TumorStage sphn:Unit sphn:VariantDescriptor sphn:Allergy sphn:AllergyEpisode sphn:CardiacIndex sphn:CardiacOutput sphn:CareHandling sphn:ChromosomalLocation sphn:Consent sphn:DataDetermination sphn:DataFile sphn:Drug sphn:DrugAdministrationEvent sphn:DrugPrescription sphn:HeartRate sphn:ICDODiagnosis sphn:LabAnalyzer sphn:LabResult sphn:Location sphn:MedicalDevice sphn:OncologyTreatmentAssessment sphn:Sample sphn:SimpleScore sphn:TimePattern sphn:TumorSpecimen adonis:SuspectedNeonatalSepsis adonis:NECDiagnosis ) ] .

sphn:hasDateTime rdfs:domain [ a owl:Class ;
            owl:unionOf ( sphn:Consent sphn:DataRelease sphn:AccessDevicePresence sphn:AdministrativeCase sphn:AdministrativeGender sphn:AdverseEvent sphn:Age sphn:Allergy sphn:AllergyEpisode sphn:BloodPressure sphn:BodyHeight sphn:BodyMassIndex sphn:BodyPosition sphn:BodySurfaceArea sphn:BodyTemperature sphn:BodyWeight sphn:CardiacIndex sphn:CardiacOutput sphn:CircumferenceMeasure sphn:CivilStatus sphn:Diagnosis sphn:DiagnosticRadiologicExamination sphn:DrugAdministrationEvent sphn:DrugPrescription sphn:ElectrocardiographicProcedure sphn:FOPHDiagnosis sphn:FOPHProcedure sphn:HealthcareEncounter sphn:HeartRate sphn:ICDODiagnosis sphn:InhaledOxygenConcentration sphn:LabResult sphn:Measurement sphn:NursingDiagnosis sphn:OncologyTreatmentAssessment sphn:OxygenSaturation sphn:ProblemCondition sphn:Procedure sphn:RadiotherapyProcedure sphn:RespiratoryRate sphn:Sample sphn:SimpleScore sphn:TNMClassification sphn:TumorGrade sphn:TumorSpecimen sphn:TumorStage adonis:NECDiagnosis ) ] .

<https://www.biomedit.ch/rdf/sphn-ontology/adonis/2023/2> a owl:Ontology ;
    dc:description "SPHN Adonis demonstrator project" ;
    dc:rights "ADONIS consortium" ;
    dc:title "ADONIS" ;
    owl:imports <http://snomed.info/sct/900000000000207008/version/20221231>,
        <https://biomedit.ch/rdf/sphn-ontology/sphn/2023/2>,
        <https://biomedit.ch/rdf/sphn-resource/atc/2023/1>,
        <https://biomedit.ch/rdf/sphn-resource/chop/2023/4>,
        sphn-geno:20220810,
        sphn-hgnc:20221107,
        <https://biomedit.ch/rdf/sphn-resource/icd-10-gm/2023/3>,
        <https://biomedit.ch/rdf/sphn-resource/loinc/2.74/1>,
        sphn-so:20211122,
        <https://biomedit.ch/rdf/sphn-resource/ucum/2023/1> ;
    owl:versionIRI <> .

adonis:hasSharedIdentifier a owl:DatatypeProperty ;
    rdfs:label "has shared identifier" ;
    rdfs:comment "identifier that is shared across data providers" ;
    rdfs:domain [ a owl:Class ;
            owl:unionOf ( sphn:SubjectPseudoIdentifier sphn:Sample ) ] ;
    rdfs:range xsd:anyURI ;
    rdfs:subPropertyOf adonis:adonisAttributeDatatype ;
    skos:definition "identifier that is shared across data providers" .

sphn:SubjectPseudoIdentifier a owl:Class ;
    rdfs:label "Subject Pseudo Identifier" ;
    rdfs:comment "a pseudo code assigned as unique identifier to an individual by a data provider institute" ;
    rdfs:subClassOf [ a owl:Class ;
            owl:intersectionOf ( [ a owl:Restriction ;
                        owl:minCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasDataProviderInstitute ] [ a owl:Restriction ;
                        owl:maxCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasDataProviderInstitute ] ) ],
        [ a owl:Class ;
            owl:intersectionOf ( [ a owl:Restriction ;
                        owl:minCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasIdentifier ] [ a owl:Restriction ;
                        owl:maxCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasIdentifier ] ) ],
        sphn:SPHNConcept ;
    skos:definition "a pseudo code assigned as unique identifier to an individual by a data provider institute" .

adonis:SuspectedNeonatalSepsis a owl:Class ;
    rdfs:subClassOf [ a owl:Class ;
            owl:intersectionOf ( [ a owl:Restriction ;
                        owl:minCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasAcquisitionMode ] [ a owl:Restriction ;
                        owl:maxCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasAcquisitionMode ] [ a owl:Restriction ;
                        owl:onProperty sphn:hasAcquisitionMode ;
                        owl:someValuesFrom [ a owl:Class ;
                                owl:unionOf ( snomed:277056009 snomed:277057000 ) ] ] ) ],
        [ a owl:Class ;
            owl:intersectionOf ( [ a owl:Restriction ;
                        owl:minCardinality "0"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasSepsisSource ] [ a owl:Restriction ;
                        owl:maxCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasSepsisSource ] [ a owl:Restriction ;
                        owl:onProperty sphn:hasSepsisSource ;
                        owl:someValuesFrom [ a owl:Class ;
                                owl:unionOf ( snomed:736152001 snomed:7180009 snomed:45170000 snomed:233604007 snomed:56819008 snomed:48661000 snomed:68566005 snomed:3723001 snomed:60168000 snomed:128045006 snomed:76844004 ) ] ] ) ],
        [ a owl:Class ;
            owl:intersectionOf ( [ a owl:Restriction ;
                        owl:minCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasSepsisClassification ] [ a owl:Restriction ;
                        owl:maxCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasSepsisClassification ] [ a owl:Restriction ;
                        owl:onProperty sphn:hasSepsisClassification ;
                        owl:someValuesFrom [ a owl:Class ;
                                owl:unionOf ( snomed:765107002 snomed:765106006 ) ] ] ) ],
        [ a owl:Class ;
            owl:intersectionOf ( [ a owl:Restriction ;
                        owl:minCardinality "0"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasPresenceOfContaminant ] [ a owl:Restriction ;
                        owl:maxCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasPresenceOfContaminant ] [ a owl:Restriction ;
                        owl:onProperty sphn:hasPresenceOfContaminant ;
                        owl:someValuesFrom snomed:66790001 ] ) ] ;
    skos:scopeNote "sphn:hasAcquisitionMode no subclasses allowed",
        "sphn:hasPresenceOfContaminant no subclasses allowed",
        "sphn:hasSepsisClassification no subclasses allowed",
        "sphn:hasSepsisSource no subclasses allowed" .

adonis:NECDiagnosis a owl:Class ;
    rdfs:subClassOf [ a owl:Class ;
            owl:intersectionOf ( [ a owl:Restriction ;
                        owl:minCardinality "0"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasAssociatedClinicalDiagnosis ] [ a owl:Restriction ;
                        owl:onProperty sphn:hasAssociatedClinicalDiagnosis ;
                        owl:someValuesFrom sphn:Code ] ) ],
        [ a owl:Class ;
            owl:intersectionOf ( [ a owl:Restriction ;
                        owl:minCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasDateTimeOfDiagnosis ] [ a owl:Restriction ;
                        owl:maxCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasDateTimeOfDiagnosis ] ) ],
        [ a owl:Class ;
            owl:intersectionOf ( [ a owl:Restriction ;
                        owl:minCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasAssociatedBellStage ] [ a owl:Restriction ;
                        owl:maxCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasAssociatedBellStage ] [ a owl:Restriction ;
                        owl:onProperty sphn:hasAssociatedBellStage ;
                        owl:someValuesFrom [ a owl:Class ;
                                owl:unionOf ( snomed:788988003 snomed:788989006 snomed:788990002 snomed:788991003 ) ] ] ) ],
        [ a owl:Class ;
            owl:intersectionOf ( [ a owl:Restriction ;
                        owl:minCardinality "0"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasAssociatedProcedure ] [ a owl:Restriction ;
                        owl:onProperty sphn:hasAssociatedProcedure ;
                        owl:someValuesFrom sphn:Code ] ) ] ;
    skos:note "sphn:hasAssociatedClinicalDiagnosis allowed coding system: SPHN",
        "sphn:hasAssociatedProcedure allowed coding system: SPHN" ;
    skos:scopeNote "sphn:hasAssociatedBellStage no subclasses allowed" .

sphn:Sample a owl:Class ;
    rdfs:label "Sample" ;
    rdfs:comment "any material sample for testing, diagnostic, propagation, treatment or research purposes" ;
    rdfs:subClassOf [ a owl:Class ;
            owl:intersectionOf ( [ a owl:Restriction ;
                        owl:minCardinality "0"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasAdministrativeCase ] [ a owl:Restriction ;
                        owl:maxCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasAdministrativeCase ] ) ],
        [ a owl:Class ;
            owl:intersectionOf ( [ a owl:Restriction ;
                        owl:minCardinality "0"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasBodySite ] [ a owl:Restriction ;
                        owl:maxCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasBodySite ] ) ],
        [ a owl:Class ;
            owl:intersectionOf ( [ a owl:Restriction ;
                        owl:minCardinality "0"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasFixationType ] [ a owl:Restriction ;
                        owl:maxCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasFixationType ] [ a owl:Restriction ;
                        owl:onProperty sphn:hasFixationType ;
                        owl:someValuesFrom sphn:Sample_fixationType ] ) ],
        [ a owl:Restriction ;
            owl:minCardinality "0"^^xsd:nonNegativeInteger ;
            owl:onProperty sphn:hasIdentifier ],
        [ a owl:Class ;
            owl:intersectionOf ( [ a owl:Restriction ;
                        owl:minCardinality "0"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasPrimaryContainer ] [ a owl:Restriction ;
                        owl:maxCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasPrimaryContainer ] [ a owl:Restriction ;
                        owl:onProperty sphn:hasPrimaryContainer ;
                        owl:someValuesFrom sphn:Sample_primaryContainer ] ) ],
        [ a owl:Class ;
            owl:intersectionOf ( [ a owl:Restriction ;
                        owl:minCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasSubjectPseudoIdentifier ] [ a owl:Restriction ;
                        owl:maxCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasSubjectPseudoIdentifier ] ) ],
        [ a owl:Class ;
            owl:intersectionOf ( [ a owl:Restriction ;
                        owl:minCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasCollectionDateTime ] [ a owl:Restriction ;
                        owl:maxCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasCollectionDateTime ] ) ],
        [ a owl:Class ;
            owl:intersectionOf ( [ a owl:Restriction ;
                        owl:minCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasDataProviderInstitute ] [ a owl:Restriction ;
                        owl:maxCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasDataProviderInstitute ] ) ],
        [ a owl:Class ;
            owl:intersectionOf ( [ a owl:Restriction ;
                        owl:minCardinality "0"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasMaterialTypeCode ] [ a owl:Restriction ;
                        owl:onProperty sphn:hasMaterialTypeCode ;
                        owl:someValuesFrom snomed:123038009 ] ) ],
        sphn:SPHNConcept ;
    skos:definition "any material sample for testing, diagnostic, propagation, treatment or research purposes" .

