# ADONIS RDF schema

This version of the ADONIS RDF schema is intended for the minimal dataset release.
As a result, it does not contain all ADONIS-specific concept but only the publicly released 2023.2 SPHN Ontology and the additional concepts: 

* Suspected Neonatal Sepsis
* NEC Diagnosis

Other ADONIS-specific concepts will be added in the upcoming versions of the RDF schema.

The schema and associated files were generated using the SPHN Schema Forge tool available online [here](https://schemaforge.dcc.sib.swiss/). The following folders are provided:

* [doc](doc): Pylode HTML file documenting the ADONIS schema.
* [schema](schema): The SPHN schema and ADONIS extension in turtle format.
* [shacl](shacl): The SHACL shapes for data validation of ADONIS data.
* [sparql](sparql): SPARQL queries to compute statistical reports on ADONIS data.
